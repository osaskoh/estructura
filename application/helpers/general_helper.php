<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function existe_cita($id_operador='',$fecha='',$hora=''){
	$ins = &get_instance();
	$ins->load->model('citas/m_citas','mc');
	return $ins->mc->existe_cita($id_operador,$fecha,$hora);
}
function datos_cita($id_horario='',$tipo){
	$ins = &get_instance();
	$ins->load->model('citas/m_citas','mc');
	return $ins->mc->datos_cita($id_horario,$tipo);
}
function getHoraComidaTecnico($hora='',$fecha='',$id_tecnico=''){
	$ins = &get_instance();
	$ins->load->model('citas/m_citas','mc');
	return $ins->mc->getHoraComidaTecnico($hora,$fecha,$id_tecnico);
}
function getHoraLaboralTecnico($hora='',$fecha='',$id_tecnico=''){
	$ins = &get_instance();
	$ins->load->model('citas/m_citas','mc');
	return $ins->mc->getHoraLaboralTecnico($hora,$fecha,$id_tecnico);
	//1
}
function getStatusCita($id_horario=''){
	$ins = &get_instance();
	$ins->load->model('citas/m_citas','mc');
	return $ins->mc->getStatusCita($id_horario);
}
function HoraBetweenCita($hora='',$fecha='',$id_tecnico=''){
	$ins = &get_instance();
	$ins->load->model('citas/m_citas','mc');
	return $ins->mc->HoraBetweenCita($hora,$fecha,$id_tecnico);
}
function getColorCita($id_cita=''){
	$ins = &get_instance();
	$ins->load->model('citas/m_citas','mc');
	return $ins->mc->getColorCita($id_cita);
}
function getHoraWork($fecha='',$idtecnico=''){
	$ins = &get_instance();
	$ins->load->model('citas/m_citas','mc');
	return $ins->mc->getHoraWork($fecha,$idtecnico);
}
function getIdIntelisis($id_cita=''){
	$ins = &get_instance();
	$ins->load->model('intelisis/m_intelisis','mi');
	return $ins->mi->getIdIntelisis($id_cita);
}
?>