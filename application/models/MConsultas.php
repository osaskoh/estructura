<?php

class MConsultas extends CI_Model{

    public function __construct()
    {
        parent::__construct();
        //Inicializamos la clase para la base de datos
        $this->load->database('default', TRUE);
    }

    public function get_table($table){
        $result = $this->db->get($table)->result();
        return $result;
    }

    public function get_table_desc(){
        $query = $this->db->query("SELECT * FROM cotizacion_multipunto ORDER BY fech_actualiza DESC");
        $result = $query->result();
        return $result;
    }

    public function get_table_desc_2(){
        $query = $this->db->query("SELECT * FROM diagnostico ORDER BY fechaRegistro DESC");
        $result = $query->result();
        return $result;
    }

    public function get_result($campo,$value,$tabla){
        $result = $this->db->where($campo,$value)->get($tabla)->result();
        return $result;
  	}

    public function audio_evi($idOren){
        $result = $this->db->where('idPivote',$idOren)->select('evidencia')->get('voz_cliente')->result();
        return $result;
  	}

    public function get_items_T1($idOrden){
        $result = $this->db->where('idorden',$idOrden)
            ->$this->db->where('tipo','1')
            ->$this->db->where('tipo','2')
            ->get('articulos_orden')->result();
        return $result;
    }

    public function get_items_T2($idOrden){
        $result = $this->db->where('idorden',$idOrden)
            ->$this->db->where('tipo','1')
            ->get('articulos_orden')->result();
        return $result;
    }

    public function last_id_table()
    {
        $query = $this->db->query("SELECT idDiagnostico FROM diagnostico_sc ORDER BY idDiagnostico DESC LIMIT 1");
        return $query;
    }

    public function last_id_table_2()
    {
        $query = $this->db->query("SELECT idDiagnostico FROM diagnostico ORDER BY idDiagnostico DESC LIMIT 1");
        return $query;
    }

    public function merge_table($id)
    {
        $query = $this->db->query("INSERT INTO diagnostico SELECT * FROM diagnostico_sc WHERE idDiagnostico = ?",array($id));
        return $query;
    }

    public function save_register($table, $data){
        $result = $this->db->insert($table, $data);
        //Comprobamos que se guarden correctamente el registro
        if ($result) {
            $result = $this->db->insert_id();
        }else {
            $result = 0;
        }
        return $result;
    }

    public function joint_table_car($id)
    {
        $sql = "SELECT o.id,o.nombre_compania,c.asesor,c.vehiculo_modelo, c.vehiculo_numero_serie, o.numero_interno AS notorre, o.color,c.datos_nombres, c.datos_apellido_paterno, c.datos_apellido_materno, c.datos_email ".
        "FROM citas AS c ".
        "INNER JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
        "WHERE c.id_cita = ?";
        $query = $this->db->query($sql,array($id));
        $data = $query->result();
    		return $data;
    }

    public function update_table_row($table,$data,$id_table,$id){
    		$result = $this->db->update($table, $data, array($id_table=>$id));
        return $result;
  	}

    public function delete_row($table,$id_table,$id){
		    $result = $this->db->delete($table, array($id_table=>$id));
        return $result;
  	}

    //funciones modelo
    public function getTipoOrden($id=''){
        $q = $this->db->where('id',$id)->select('tipo_orden')->get('cat_tipo_orden');
        if($q->num_rows()==1){
            $retorno = $q->row()->tipo_orden;
        }else{
            $retorno = '';
        }
        return $retorno;
    }

    //Obtiene comentario cita
    public function getColorCitaAuto($id=''){
        $q = $this->db->where('id_cita',$id)->select("ct.color")->join('cat_colores ct','c.id_color = ct.id')->where('c.activo',1)->get('citas c ');
        if($q->num_rows()==1){
            $retorno = $q->row()->color;
        }else{
            $retorno = '';
        }
          return $retorno;
    }

    public function getAnticipoOrden($idOrden = 0){
        $q = $this->db->where('id',$idOrden)->select('anticipo')->get('ordenservicio');
        if($q->num_rows()==1){
            $retorno = (int)$q->row()->anticipo;
        }else{
            $retorno = 0;
        }
        return $retorno;
    }

   //Obtiene si la cotización fue aceptada o no
    public function EstatusCotizacionUser($idorden=''){
       $q = $this->db->where('id',$idorden)->select('cotizacion_confirmada')->get('ordenservicio');
        if($q->num_rows()==1){
            $retorno = $q->row()->cotizacion_confirmada;
        }else{
            $retorno = '';
        }
        return $retorno;
    }

    public function citas($idorden='')
    {
        $query = $this->db->where('o.id',$idorden)
            ->join('citas AS c','c.id_cita=o.id_cita')
            ->join('cat_tipo_pago AS cp','o.id_tipo_pago=cp.id','left')
            ->select('o.*,c.*,cp.tipo_pago')
            ->get('ordenservicio AS o')
            ->result();
        return $query;
    }

    public function tecnicos($idCita='')
    {
        $query = $this->db->where('tc.id_cita',$idCita)
            ->join('tecnicos AS t','tc.id_tecnico=t.id')
            ->select('tc.*,t.nombre AS tecnico')
            ->get('tecnicos_citas AS tc')
            ->result();
        return $query;
    }

    public function apiRequest()
    {
        //cat_monedas
        $sql = "SELECT  o.*,c.* ".
        "FROM citas AS c ".
        "LEFT JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
        "ORDER BY o.id DESC LIMIT 50";
        // "WHERE c.id_cita = ?";
        // $query = $this->db->query($sql,array($id));
        $query = $this->db->query($sql);
        $data = $query->result();
        return $data;
    }

    public function apiRequest_2()
    {
        //cat_monedas
        $sql = "SELECT  o.*,c.*,t.nombre AS tecnico, t.clave AS clave_tecnico,cat.categoria, subcat.subcategoria ".
        "FROM citas AS c ".
        "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
        "LEFT JOIN ordenservicio AS ON o.id_cita = c.id_cita ".
        "LEFT JOIN categorias AS cat ON cat.id = o.idcategoria ".
        "LEFT JOIN subcategorias AS subcat ON subcat.id = o.idsubcategoria ".
        "ORDER BY o.id DESC LIMIT 50";
        // "WHERE c.id_cita = ?";
        // $query = $this->db->query($sql,array($id));
        $query = $this->db->query($sql);
        $data = $query->result();
        return $data;
    }

    //Teléfono del asesor
    public function telefono_asesor($idorden=''){
        $q = $this->db->where('id_cita',$idorden)->select('telefono_asesor')->from('ordenservicio')->get();
        if($q->num_rows()==1){
            return $q->row()->telefono_asesor;
        }else{
            return '';
        }
    }

    //Teléfono del tecnico
    public function telefono_tecnico($idorden=''){
        $query = $this->db->where('c.id_cita',$idorden)
                  ->join('tecnicos AS t','c.id_tecnico = t.id')
                  ->select('t.tel')->from('citas AS c')->get();
        if($query->num_rows() == 1){
            return $query->row()->tel;
        }else{
            return '';
        }
    }

    public function precargaConsulta($idOrden = 0)
    {
        $sql = "SELECT  o.*,c.*,t.nombre AS tecnico, t.clave AS clave_tecnico,cat.categoria, subcat.subcategoria ".
        "FROM citas AS c ".
        "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
        "LEFT JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
        "LEFT JOIN categorias AS cat ON cat.id = o.idcategoria ".
        "LEFT JOIN subcategorias AS subcat ON subcat.id = o.idsubcategoria ".
        "WHERE c.id_cita = ?";
        $query = $this->db->query($sql,array($idOrden));
        $data = $query->result();
        return $data;
    }

    public function articulosTitulos($idOrden = 0)
    {
        $sql = "SELECT atr.*,gps.clave AS grupoDes, opgps.articulo AS operacionDes ".
        "FROM articulos_orden AS atr ".
        "LEFT JOIN grupo_operacion AS opgps ON opgps.id = atr.grupo ".
        "LEFT JOIN grupos AS gps ON gps.id = opgps.idgrupo ".
        "WHERE atr.idorden = ?";
        $query = $this->db->query($sql,array($idOrden));
        $data = $query->result();
        return $data;
    }

    public function last_registre()
    {
        $query = $this->db->query("SELECT idRegistro FROM formulario_quere1 ORDER BY idRegistro DESC LIMIT 1");
        return $query;
    }

    public function Unidad_entregada($id_cita=''){
        $q = $this->db->where('id_cita',$id_cita)->select('unidad_entregada')->get('citas');
        if($q->num_rows()==1){
            $respuesta = $q->row()->unidad_entregada;
        }else{
            $respuesta = '0';
        }
        return $respuesta;
    }

    public function tecnicosOrdenes($idTecnico = 0)
    {
        $sql = "SELECT  t.nombre AS tecnico, o.id_cita AS orden_cita, d.archivoOasis, c.asesor,c.datos_nombres,c.datos_apellido_paterno,c.datos_apellido_materno, o.folioIntelisis ".
        "FROM citas AS c ".
        "INNER JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
        "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
        "LEFT JOIN diagnostico AS d ON d.noServicio = c.id_cita ".
        "WHERE c.id_tecnico = ? ".
        "ORDER BY o.fecha_recepcion DESC ".
        "LIMIT 300 ";
        $query = $this->db->query($sql,array($idTecnico));
        $data = $query->result();
        return $data;
    }

    public function listaOrdenes()
    {
        $sql = "SELECT  t.nombre AS tecnico, o.id_cita AS orden_cita, d.archivoOasis, c.asesor,c.datos_nombres,c.datos_apellido_paterno,c.datos_apellido_materno, o.folioIntelisis ".
        "FROM citas AS c ".
        "INNER JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
        "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
        "LEFT JOIN diagnostico AS d ON d.noServicio = c.id_cita ".
        "ORDER BY o.fecha_recepcion DESC ".
        "LIMIT 300 ";
        // "WHERE c.id_tecnico = ?";
        $query = $this->db->query($sql,array());
        $data = $query->result();
        return $data;
    }

    public function get_table_list($table,$campo){
        $result = $this->db->order_by($campo, "DESC")
            ->get($table)
            ->result();
        return $result;
    }

    public function api_orden($idOrden = 0)
    {
        $sql = "SELECT  o.*,c.*,o.id_cita as id_Cita_orden,t.nombre AS tecnico, t.clave AS clave_tecnico,cat.categoria, subcat.subcategoria, ".
        // "st.status AS estatus_proceso, sit.situacion, stod.status AS estatus_orden, ".
        "o.numero_cliente as num_Cliente_orden, c.numero_cliente as num_Cliente_cita ".
        "FROM citas AS c ".
        "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
        "LEFT JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
        "LEFT JOIN categorias AS cat ON cat.id = o.idcategoria ".
        "LEFT JOIN subcategorias AS subcat ON subcat.id = o.idsubcategoria ".
        //Estatus para intelisis
        // "LEFT JOIN cat_status_intelisis_orden AS st ON st.id = o.id_status_intelisis ".
        // "LEFT JOIN cat_situacion_intelisis AS sit ON sit.id = o.id_situacion_intelisis ".
        // "LEFT JOIN cat_status_orden AS stod ON stod.id = o.id_status ".
        "WHERE c.id_cita = ?";
        $query = $this->db->query($sql,array($idOrden));

        $data = $query->result();
        return $data;
    }

    //Consultas para indices de tabla
    //Consula para filtros de refacciones
    public function filtro_busqueda($valor = "",$tabla = "",$campo_1="",$campo_2="")
    {
        // $this->db->distinct('clave_comprimida');
        $this->db->select('*');
        $this->db->from($tabla);
        $this->db->like('descripcion',$valor,'both');
        $this->db->or_like('clave_comprimida',$valor,'both');

        $this->db->order_by('descripcion', "ASC");
        $query = $this->db->get()->result();
        return $query;
    }

    //Recuperar una tabla en orden asecendente limitada a 1000 registros
    public function get_table_order_limit($table="",$campo="",$inicio=0,$fin = 0){
        $this->db->order_by($campo, "DESC");
        $this->db->limit($fin,$inicio);
        $result = $this->db->get($table)->result();
        return $result;
    }

    //Recuperar total de registros
    public function conteo_registros($table ="") {
        $this->db->from($table);
        return $num_rows = $this->db->count_all_results();
    }

    public function tecnicosOrdenes_Conteo($idTecnico = 0)
    {
        $sql = "SELECT  d.*,t.nombre AS tecnico ".
        "FROM citas AS c ".
        "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
        "LEFT JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
        "INNER JOIN diagnostico AS d ON d.noServicio = c.id_cita ".
        "WHERE c.id_tecnico = ? ".
        "ORDER BY d.fechaRegistro DESC ";
        $query = $this->db->query($sql,array($idTecnico));
        $data = $this->db->count_all_results();
        return $data;
    }

    public function listaOrdenes_Conteo()
    {
        $sql = "SELECT  d.*,t.nombre AS tecnico ".
        "FROM citas AS c ".
        "INNER JOIN tecnicos AS t ON c.id_tecnico = t.id ".
        "INNER JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
        "INNER JOIN diagnostico AS d ON d.noServicio = c.id_cita ".
        "ORDER BY d.fechaRegistro DESC ";
        // "WHERE c.id_tecnico = ?";
        $query = $this->db->query($sql,array());
        $data = $this->db->count_all_results();
        return $data;
    }

    public function tecnicosOrdenes_Limite($idTecnico = 0,$inicio=0,$fin = 0)
    {
        $sql = "SELECT  d.*,t.nombre AS tecnico ".
        "FROM citas AS c ".
        "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
        "LEFT JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
        "INNER JOIN diagnostico AS d ON d.noServicio = c.id_cita ".
        "WHERE c.id_tecnico = ? ".
        "ORDER BY d.fechaRegistro DESC ".
        "LIMIT ?,? ";
        $query = $this->db->query($sql,array($idTecnico,$inicio,$fin));
        $data = $this->db->count_all_results();
        return $data;
    }

    public function listaOrdenes_Limite($inicio=0,$fin = 0)
    {
        $sql = "SELECT  d.*,t.nombre AS tecnico ".
        "FROM citas AS c ".
        "INNER JOIN tecnicos AS t ON c.id_tecnico = t.id ".
        "INNER JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
        "INNER JOIN diagnostico AS d ON d.noServicio = c.id_cita ".
        "ORDER BY d.fechaRegistro DESC ".
        "LIMIT ?,? ";
        // "WHERE c.id_tecnico = ?";
        $query = $this->db->query($sql,array($inicio,$fin));
        $data = $this->db->count_all_results();
        return $data;
    }

    public function multipuntoApi($idOrden = 0)
    {
        $sql = "SELECT o.idVentaIntelisis,mg.id,mg.bateriaEstado,mg.bateriaCambio,mg.bateria,mg3.tecnico, ".
        "fi.espesoBalataFI,fi.espesorBalataFIcambio,fi.espesorBalataFImm,fi.espesorDiscoFI,fi.espesorDiscoFIcambio,fi.espesorDiscoFImm, ".
        "ti.espesoBalataTI,ti.espesorBalataTIcambio,ti.espesorBalataTImm,ti.espesorDiscoTI,ti.espesorDiscoTIcambio,ti.espesorDiscoTImm,ti.diametroTamborTI,ti.diametroTamborTIcambio, ".
        "fd.espesoBalataFD,fd.espesorBalataFDcambio,fd.espesorBalataFDmm,fd.espesorDiscoFD,fd.espesorDiscoFDcambio,fd.espesorDiscoFDmm, ".
        "td.espesoBalataTD,td.espesorBalataTDcambio,td.espesorBalataTDmm,td.espesorDiscoTD,td.espesorDiscoTDcambio,td.espesorDiscoTDmm,td.diametroTamborTD,td.diametroTamborTDcambio ".
        "FROM multipunto_general AS mg ".
        "LEFT JOIN multipunto_general_3 AS mg3 ON mg3.idOrden = mg.id ".
        "LEFT JOIN frente_izquierdo AS fi ON fi.idOrden = mg.id ".
        "LEFT JOIN trasero_izquierdo AS ti ON ti.idOrden = mg.id ".
        "LEFT JOIN frente_derecho AS fd ON fd.idOrden = mg.id ".
        "LEFT JOIN trasero_derecho AS td ON td.idOrden = mg.id ".
        "LEFT JOIN ordenservicio AS o ON o.id_cita = mg.orden ".
        "WHERE mg.orden = ?";

        $query = $this->db->query($sql,array($idOrden));
        $data = $query->result();
        return $data;
    }

    //Recuperamos el id de la orden de intelisis
    public function id_orden_Intelisis($idorden=''){
        $q = $this->db->where('id_cita',$idorden)->select('folioIntelisis')->from('ordenservicio')->get();
        if($q->num_rows()==1){
            return $q->row()->folioIntelisis;
        }else{
            return '';
        }
    }

    //Recuperamos el id de la orden de intelisis
    public function ids_intelisis($idorden=''){
        $q = $this->db->where('id_cita',$idorden)->select('idIntelisis','idVentaIntelisis')->from('ordenservicio')->get();
        $data = $q->result();

        return $data;
    }

    public function listaOrdenes_historial(){
        $sql = "SELECT  t.nombre AS tecnico, o.id_cita AS orden_cita, d.archivoOasis, c.asesor,c.datos_nombres,c.datos_apellido_paterno,c.datos_apellido_materno ".
        "FROM citas AS c ".
        "INNER JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
        "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
        "LEFT JOIN diagnostico AS d ON d.noServicio = c.id_cita ".
        "ORDER BY o.fecha_recepcion DESC ".
        "LIMIT 300 ";
        // "WHERE c.id_tecnico = ?";
        $query = $this->db->query($sql,array());
        $data = $query->result();
        return $data;
    }

    //Recuperamos la informacion de los indicadores de una multipunto
    public function multipuntoIndicador($idOrden = 0)
    {
        $sql = "SELECT o.fecha_recepcion,mg.orden,mg.bateriaEstado,mg3.asesor,mg3.nombreCliente, c.datos_email, c.datos_telefono,mg.noSerie, ".
        "fi.dNeumaticoFI,fi.espesoBalataFI,fi.espesorDiscoFI, ".
        "ti.dNeumaticoTI,ti.espesoBalataTI,ti.espesorDiscoTI,ti.diametroTamborTI, ".
        "fd.dNeumaticoFD,fd.espesoBalataFD,fd.espesorDiscoFD, ".
        "td.dNeumaticoTD,td.espesoBalataTD,td.espesorDiscoTD,td.diametroTamborTD ".
        "FROM multipunto_general AS mg ".
        "LEFT JOIN multipunto_general_3 AS mg3 ON mg3.idOrden = mg.id ".
        "LEFT JOIN frente_izquierdo AS fi ON fi.idOrden = mg.id ".
        "LEFT JOIN trasero_izquierdo AS ti ON ti.idOrden = mg.id ".
        "LEFT JOIN frente_derecho AS fd ON fd.idOrden = mg.id ".
        "LEFT JOIN trasero_derecho AS td ON td.idOrden = mg.id ".
        "LEFT JOIN ordenservicio AS o ON o.id_cita = mg.orden ".
        "WHERE mg.orden = ?";

        $query = $this->db->query($sql,array($idOrden));
        $data = $query->result();
        return $data;
    }

    //Recuperamos la infotmacion de las multipunto con indicadores
    public function multipuntoListaIndicador()
    {
        $sql = "SELECT o.fecha_recepcion,mg.orden,mg.bateriaEstado,mg3.asesor,mg3.nombreCliente, c.datos_email, c.datos_telefono,mg.noSerie, ".
        "fi.dNeumaticoFI,fi.espesoBalataFI,fi.espesorDiscoFI, ".
        "ti.dNeumaticoTI,ti.espesoBalataTI,ti.espesorDiscoTI,ti.diametroTamborTI, ".
        "fd.dNeumaticoFD,fd.espesoBalataFD,fd.espesorDiscoFD, ".
        "td.dNeumaticoTD,td.espesoBalataTD,td.espesorDiscoTD,td.diametroTamborTD ".
        "FROM multipunto_general AS mg ".
        "LEFT JOIN multipunto_general_3 AS mg3 ON mg3.idOrden = mg.id ".
        "LEFT JOIN frente_izquierdo AS fi ON fi.idOrden = mg.id ".
        "LEFT JOIN trasero_izquierdo AS ti ON ti.idOrden = mg.id ".
        "LEFT JOIN frente_derecho AS fd ON fd.idOrden = mg.id ".
        "LEFT JOIN trasero_derecho AS td ON td.idOrden = mg.id ".
        "LEFT JOIN ordenservicio AS o ON o.id_cita = mg.orden ".
        "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
        "ORDER BY o.fecha_recepcion DESC LIMIT 300";

        $query = $this->db->query($sql,array());
        $data = $query->result();
        return $data;
    }

    //Recuperamos la infotmacion de las multipunto con indicadores entre fechas
    public function multipuntoListaIndicadorFechas($fech1 = "",$fech2 = "")
    {
        $sql = "SELECT o.fecha_recepcion,mg.orden,mg.bateriaEstado,mg3.asesor,mg3.nombreCliente, c.datos_email, c.datos_telefono,mg.noSerie, ".
        "fi.dNeumaticoFI,fi.espesoBalataFI,fi.espesorDiscoFI, ".
        "ti.dNeumaticoTI,ti.espesoBalataTI,ti.espesorDiscoTI,ti.diametroTamborTI, ".
        "fd.dNeumaticoFD,fd.espesoBalataFD,fd.espesorDiscoFD, ".
        "td.dNeumaticoTD,td.espesoBalataTD,td.espesorDiscoTD,td.diametroTamborTD ".

        "FROM multipunto_general AS mg ".
        "LEFT JOIN multipunto_general_3 AS mg3 ON mg3.idOrden = mg.id ".
        "LEFT JOIN frente_izquierdo AS fi ON fi.idOrden = mg.id ".
        "LEFT JOIN trasero_izquierdo AS ti ON ti.idOrden = mg.id ".
        "LEFT JOIN frente_derecho AS fd ON fd.idOrden = mg.id ".
        "LEFT JOIN trasero_derecho AS td ON td.idOrden = mg.id ".
        "LEFT JOIN ordenservicio AS o ON o.id_cita = mg.orden ".
        "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".

        "WHERE o.created_at BETWEEN ? AND ? ".
        "ORDER BY o.fecha_recepcion DESC";

        $query = $this->db->query($sql,array($fech1,$fech2));
        $data = $query->result();
        return $data;
    }

    //Recuperamos la infotmacion de las multipunto con indicadores entre fechas
    public function multipuntoListaIndicadorGral($comodin = "")
    {
        $sql = "SELECT o.fecha_recepcion,mg.orden,mg.bateriaEstado,mg3.asesor,mg3.nombreCliente, c.datos_email, c.datos_telefono,mg.noSerie, ".
        "fi.dNeumaticoFI,fi.espesoBalataFI,fi.espesorDiscoFI, ".
        "ti.dNeumaticoTI,ti.espesoBalataTI,ti.espesorDiscoTI,ti.diametroTamborTI, ".
        "fd.dNeumaticoFD,fd.espesoBalataFD,fd.espesorDiscoFD, ".
        "td.dNeumaticoTD,td.espesoBalataTD,td.espesorDiscoTD,td.diametroTamborTD ".

        "FROM multipunto_general AS mg ".
        "LEFT JOIN multipunto_general_3 AS mg3 ON mg3.idOrden = mg.id ".
        "LEFT JOIN frente_izquierdo AS fi ON fi.idOrden = mg.id ".
        "LEFT JOIN trasero_izquierdo AS ti ON ti.idOrden = mg.id ".
        "LEFT JOIN frente_derecho AS fd ON fd.idOrden = mg.id ".
        "LEFT JOIN trasero_derecho AS td ON td.idOrden = mg.id ".
        "LEFT JOIN ordenservicio AS o ON o.id_cita = mg.orden ".
        "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".

        "WHERE mg.orden LIKE ? ".
        "OR mg.noSerie LIKE ? ".
        "OR mg3.asesor LIKE ? ".
        "OR mg3.nombreCliente LIKE ? ".
        "OR c.datos_telefono LIKE ? ".
        "ORDER BY o.fecha_recepcion DESC";

        $query = $this->db->query($sql,array("%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%"));
        $data = $query->result();
        return $data;
    }

    //Recuperamos el id del historial proactivo
    public function idHistorialProactivo($serie = '')
    {
        $q = $this->db->where('serie',$serie)->select('id')->limit(1)->get('magic');
        if($q->num_rows()==1){
            $respuesta = $q->row()->id;
        }else{
            $respuesta = '0';
        }
        return $respuesta;
    }

    //Recuperamos las cotizaciones rechazadas
    public function quotesList()
    {
        $sql = "SELECT c.asesor,o.id_cita,o.fecha_recepcion,c.asesor,c.datos_nombres,c.datos_apellido_paterno,c.datos_apellido_materno, cm.aceptoTermino,cm.identificador,c.vehiculo_numero_serie,o.vehiculo_identificacion,cm.contenido ".
        "FROM ordenservicio AS o ".
        "INNER JOIN cotizacion_multipunto AS cm ON o.id_cita = cm.idOrden ".
        "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
        "WHERE cm.aceptoTermino != ? AND cm.aceptoTermino != ?".
        "ORDER BY o.fecha_recepcion DESC ".
        "LIMIT 200";

        $query = $this->db->query($sql,array('Si',''));
        $data = $query->result();
        return $data;
    }

    //Recuperamos el id del estatus en color de servicio
    public function tipo_servicio($idorden=''){
        $q = $this->db->where('id_cita',$idorden)->select('id_status_color')->from('citas')->get();
        if($q->num_rows()==1){
            $respuesta = $q->row()->id_status_color;
        }else{
            $respuesta = '';
        }
        return $respuesta;
    }

    //Filtros para magic
    public function busquedaMagicCampo($comodin = '')
    {
        $sql = "SELECT id FROM magic ".
        "WHERE nombre LIKE ? OR ap LIKE ? OR am LIKE ? OR calle LIKE ? OR colonia LIKE ? ".
        "OR cp LIKE ? OR municipio LIKE ? OR tel_principal LIKE ? OR tel_adicional LIKE ? OR dirigirse_con LIKE ? OR rfc LIKE ? ".
        "OR correo LIKE ? OR tel_secundario LIKE ? OR notorre LIKE ? OR km_actual LIKE ? OR placas LIKE ? OR no_siniestro LIKE ? ".
        "OR no_orden LIKE ? OR serie_larga LIKE ? OR codigoCte LIKE ? OR razon_social LIKE ? ".
        "ORDER BY fecha_recepcion DESC";

        $contenido = array("%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%",
            "%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%",
            "%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%");

        $query = $this->db->query($sql,$contenido);
        $data = $query->result();
        return $data;
    }

    public function busquedaMagicFecha($fech1 = '',$fech2 = '')
    {
        $sql = "SELECT id FROM magic WHERE fecha_recepcion BETWEEN ? AND ? ORDER BY fecha DESC";

        $query = $this->db->query($sql,array($fech1,$fech2));
        $data = $query->result();
        return $data;
    }

    public function busquedaMagicTodo($comodin = '',$fech1 = '',$fech2 = '')
    {
        $sql = "SELECT id FROM magic ".
        "WHERE nombre LIKE ? OR ap LIKE ? OR am LIKE ? OR calle LIKE ? OR colonia LIKE ? ".
        "OR cp LIKE ? OR municipio LIKE ? OR tel_principal LIKE ? OR tel_adicional LIKE ? OR dirigirse_con LIKE ? OR rfc LIKE ? ".
        "OR correo LIKE ? OR tel_secundario LIKE ? OR notorre LIKE ? OR km_actual LIKE ? OR placas LIKE ? OR no_siniestro LIKE ? ".
        "OR no_orden LIKE ? OR serie_larga LIKE ? OR codigoCte LIKE ? OR razon_social LIKE ? AND fecha_recepcion BETWEEN ? AND ? ".
        "ORDER BY fecha_recepcion DESC";

        $contenido = array("%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%",
            "%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%",
            "%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%",$fech1,$fech2);

        $query = $this->db->query($sql,$contenido);
        $data = $query->result();
        return $data;

    }

    //Recuperamos el id del historial proactivo
    public function nombreOperador($id = '')
    {
        $q = $this->db->where('id',$id)->select('nombre')->get('operadores');
        if($q->num_rows()==1){
            $retorno = $q->row()->nombre;
        }else{
            $retorno = '';
        }
        return $retorno;
    }

    //Recuperamos el id del historial proactivo
    public function idFichaPerfil($idPerfil = '')
    {
        $q = $this->db->where('id',$idPerfil)->select('*')->limit(1)->get('magic')->result();
        return $q;
    }

    public function getTipoOrdenIdentificacion($id=''){
        $q = $this->db->where('id',$id)->select('identificador')->get('cat_tipo_orden');
        if($q->num_rows()==1){
            $retorno = $q->row()->identificador;
        }else{
            $retorno = '';
        }
        return $retorno;
    }

    //----
    public function last_id_tableVC()
    {
        $query = $this->db->query("SELECT idRegistro FROM voz_cliente_temp ORDER BY idRegistro DESC LIMIT 1");
        return $query;
    }

    public function last_id_tableVC_2()
    {
        $query = $this->db->query("SELECT idRegistro FROM voz_cliente ORDER BY idRegistro DESC LIMIT 1");
        return $query;
    }

    public function merge_tableVC($id = 0)
    {
        $query = $this->db->query("INSERT INTO voz_cliente SELECT * FROM voz_cliente_temp WHERE idRegistro = ?",array($id));
        return $query;
    }

    public function get_table_desc_VC(){
        $query = $this->db->query("SELECT * FROM voz_cliente ORDER BY fecha_registro DESC");
        $result = $query->result();
        return $result;
    }

    public function dataMultipuntoTec($idTecnico = 0)
    {
        $query = $this->db->where("t.id",$idTecnico)
            ->join('multipunto_general_3 AS mg3','mg3.idOrden = mg.id')
            ->join('citas AS c','c.id_cita = mg.orden')
            ->join('tecnicos AS t','c.id_tecnico = t.id')
            ->select('mg3.comentario,mg3.sistema,mg3.componente,mg3.causaRaiz,mg3.asesor,mg3.nombreCliente,mg.orden,mg.fechaCaptura')
            ->order_by("mg.fechaCaptura", "DESC")
            ->get('multipunto_general AS mg')
            ->result();
        return $query;
    }

    public function dataMultipunto()
    {
        $query = $this->db->join('multipunto_general_3 AS mg3','mg3.idOrden = mg.id')
            // ->join('citas AS c','c.id_cita = mg.orden')
            // ->join('tecnicos AS t','c.id_tecnico = t.id')
            ->select('mg3.comentario,mg3.sistema,mg3.componente,mg3.causaRaiz,mg3.asesor,mg3.nombreCliente,mg.orden,mg.fechaCaptura')
            ->order_by("mg.fechaCaptura", "DESC")
            ->get('multipunto_general AS mg')
            ->result();
        return $query;
    }

    //Filtros para magic actualizado (ordenes)
    public function busquedaMagic_CitasCampo($comodin = '')
    {
        $sql = "SELECT c.id_cita FROM citas AS c ".
        "INNER JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
        "WHERE c.email LIKE ? OR c.vehiculo_anio LIKE ? OR c.vehiculo_modelo LIKE ? OR c.vehiculo_version LIKE ? OR ".
        "c.vehiculo_placas LIKE ? OR c.vehiculo_numero_serie LIKE ? OR c.comentarios_servicio LIKE ? OR c.asesor LIKE ? OR ".
        "c.datos_email LIKE ? OR c.datos_nombres LIKE ? OR c.datos_apellido_paterno LIKE ? OR c.datos_apellido_materno LIKE ? OR ".
        "c.datos_telefono LIKE ? OR c.servicio LIKE ? OR c.ubicacion_unidad LIKE ? OR c.numero_cliente LIKE ? OR ".
        "o.telefono_asesor LIKE ? OR o.extension_asesor LIKE ? OR o.numero_interno LIKE ? OR o.nombre_compania LIKE ? OR o.nombre_contacto_compania LIKE ? OR ".
        "o.am_contacto LIKE ? OR o.rfc LIKE ? OR o.correo_compania LIKE ? OR o.calle LIKE ? OR o.nointerior LIKE ? OR o.noexterior LIKE ? OR ".
        "o.colonia LIKE ? OR o.municipio LIKE ? OR o.cp LIKE ? OR o.estado LIKE ? OR o.telefono_movil LIKE ? OR o.otro_telefono LIKE ? OR o.ap_contacto LIKE ? OR ".
        "o.vehiculo_identificacion LIKE ? OR o.vehiculo_kilometraje LIKE ? OR o.motor LIKE ? OR o.transmision LIKE ? OR o.numero_cliente LIKE ? ".
        "ORDER BY o.fecha_recepcion DESC LIMIT 40";

        $contenido = array("%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%",
            "%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%",
            "%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%",
            "%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%",
            "%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%");

        $query = $this->db->query($sql,$contenido);
        $data = $query->result();
        return $data;
    }

    public function busquedaMagic_CitasFecha($fech1 = '0000-00-00',$fech2 = '0000-00-00')
    {
        $sql = "SELECT id_cita FROM ordenservicio WHERE fecha_recepcion BETWEEN ? AND ? ORDER BY fecha_recepcion DESC LIMIT 40";

        $query = $this->db->query($sql,array($fech1,$fech2));
        $data = $query->result();
        return $data;
    }

    public function busquedaMagic_CitasTodo($comodin = '',$fech1 = '0000-00-00',$fech2 = '0000-00-00')
    {
        $sql = "SELECT c.id_cita FROM citas AS c ".
        "INNER JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
        "WHERE c.email LIKE ? OR c.vehiculo_anio LIKE ? OR c.vehiculo_modelo LIKE ? OR c.vehiculo_version LIKE ? OR ".
        "c.vehiculo_placas LIKE ? OR c.vehiculo_numero_serie LIKE ? OR c.comentarios_servicio LIKE ? OR c.asesor LIKE ? OR ".
        "c.datos_email LIKE ? OR c.datos_nombres LIKE ? OR c.datos_apellido_paterno LIKE ? OR c.datos_apellido_materno LIKE ? OR ".
        "c.datos_telefono LIKE ? OR c.servicio LIKE ? OR c.ubicacion_unidad LIKE ? OR c.numero_cliente LIKE ? OR ".
        "o.telefono_asesor LIKE ? OR o.extension_asesor LIKE ? OR o.numero_interno LIKE ? OR o.nombre_compania LIKE ? OR o.nombre_contacto_compania LIKE ? OR ".
        "o.am_contacto LIKE ? OR o.rfc LIKE ? OR o.correo_compania LIKE ? OR o.calle LIKE ? OR o.nointerior LIKE ? OR o.noexterior LIKE ? OR ".
        "o.colonia LIKE ? OR o.municipio LIKE ? OR o.cp LIKE ? OR o.estado LIKE ? OR o.telefono_movil LIKE ? OR o.otro_telefono LIKE ? OR o.ap_contacto LIKE ? OR ".
        "o.vehiculo_identificacion LIKE ? OR o.vehiculo_kilometraje LIKE ? OR o.motor LIKE ? OR o.transmision LIKE ? OR o.numero_cliente LIKE ? ".
        "OR o.fecha_recepcion BETWEEN ? AND ? ORDER BY o.fecha_recepcion DESC LIMIT 40";

        $contenido = array("%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%",
        "%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%",
        "%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%",
        "%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%",
        "%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%",$fech1,$fech2);

        $query = $this->db->query($sql,$contenido);
        $data = $query->result();
        return $data;

    }

    //Consulta para ordenes entregadas en dia de hoy
    public function orderClosed($fecha = "")
    {
        $sql = "SELECT o.id_cita FROM ordenservicio AS o INNER JOIN citas AS c ON c.id_cita = o.id_cita WHERE c.unidad_entregada = 1 AND o.fecha_entrega = ?";
        $query = $this->db->query($sql,array($fecha));
        $data = $query->result();
        return $data;
    }

    public function tecnicos_citas($idCita = ""){
        $sql = "SELECT id,fecha, hora_inicio_dia, fecha_fin, hora_fin FROM tecnicos_citas WHERE id_cita = ? ORDER BY id ASC";
        $query = $this->db->query($sql,$idCita);
        $data = $query->result();
        return $data;
    }

    //Recuperar informacion de la orden
    public function notificacionTecnicoCita($idOrden = '')
    {
        $sql = "SELECT tc.*, st.nombre, c.comentarios_servicio, catOp.tipo_operacion ".  
              "FROM ordenservicio AS o ".
              "INNER JOIN citas AS c ON c.id_cita = o.id_cita ".
              "INNER JOIN tecnicos_citas AS tc ON tc.id_cita = o.id_cita ".
              "INNER JOIN estatus AS st ON st.id = c.id_status_color ".
              "INNER JOIN cat_tipo_operacion AS catOp ON catOp.id = o.id_tipo_operacion ".
              "WHERE o.id_cita = ? LIMIT 1";
            
        $query = $this->db->query($sql,$idOrden);
        $data = $query->result();
        return $data;
    }

    public function update_table_row_2($table,$data,$id_table,$id,$id_table_2,$id_2){
        $result = $this->db->update($table, $data, array($id_table=>$id,$id_table_2=>$id_2));
        return $result;
    }

    public function get_result_not($value = ""){
        $sql = "SELECT c.*, o.* FROM ordenservicio AS o INNER JOIN citas AS c ON c.id_cita = o.id_cita WHERE errorEnvio != ? ORDER BY o.fecha_recepcion DESC LIMIT 50";
        $query = $this->db->query($sql,$value);
        $data = $query->result();
        return $data;
    }


}
